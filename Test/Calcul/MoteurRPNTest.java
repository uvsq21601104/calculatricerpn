package Calcul;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class MoteurRPNTest {

    private MoteurRPN test;

    @Before
    public void Before(){
        test = new MoteurRPN();


    }


    @Test
    public void AddingValueTest(){
        try {
            this.test.Enregistrer(5);
            this.test.Enregistrer(2);
        } catch (FullStackException e) {
            e.printStackTrace();
        } catch (OutOfBoundsException e) {
            e.printStackTrace();
        }
        assertTrue(this.test.Retourner().get(0)==5);
        assertTrue(this.test.Retourner().get(1)==2);


    }


    @Test
    public void PopingValueTest(){
        try {
            this.test.Enregistrer(5);
            this.test.Enregistrer(2);
        } catch (FullStackException e) {
            e.printStackTrace();
        } catch (OutOfBoundsException e) {
            e.printStackTrace();
        }
        try {
            this.test.Depiler();
            this.test.Depiler();
        } catch (EmptyStackException e) {
            e.printStackTrace();
        }

        assertTrue(this.test.Retourner().isEmpty());



    }


    @Test
    public void AdditionTest(){

        try {
            this.test.Enregistrer(5);
            this.test.Enregistrer(2);
        } catch (FullStackException e) {
            e.printStackTrace();
        } catch (OutOfBoundsException e) {
            e.printStackTrace();
        }
        try {
            this.test.Application(Operation.PLUS);
        } catch (DivisionZeroException e) {
            e.printStackTrace();
        } catch (EmptyStackException e) {
            e.printStackTrace();
        } catch (OutOfBoundsException e) {
            e.printStackTrace();
        }
        assertTrue(this.test.Retourner().get(this.test.Retourner().size()-1)==7);
    }
    @Test
    public void MinusTest(){

        try {
            this.test.Enregistrer(5);
            this.test.Enregistrer(2);
        } catch (FullStackException e) {
            e.printStackTrace();
        } catch (OutOfBoundsException e) {
            e.printStackTrace();
        }
        try {
            this.test.Application(Operation.MOINS);
        } catch (DivisionZeroException e) {
            e.printStackTrace();
        } catch (EmptyStackException e) {
            e.printStackTrace();
        } catch (OutOfBoundsException e) {
            e.printStackTrace();
        }
        assertTrue(this.test.Retourner().get(this.test.Retourner().size()-1)==-3);
    }
    @Test
    public void MultiplicationTest(){

        try {
            this.test.Enregistrer(5);
            this.test.Enregistrer(2);
        } catch (FullStackException e) {
            e.printStackTrace();
        } catch (OutOfBoundsException e) {
            e.printStackTrace();
        }
        try {
            this.test.Application(Operation.MULT);
        } catch (DivisionZeroException e) {
            e.printStackTrace();
        } catch (EmptyStackException e) {
            e.printStackTrace();
        } catch (OutOfBoundsException e) {
            e.printStackTrace();
        }
        assertTrue(this.test.Retourner().get(this.test.Retourner().size()-1)==10);
    }
    @Test
    public void DivisionTest(){

        try {
            this.test.Enregistrer(5);
            this.test.Enregistrer(2);
        } catch (FullStackException e) {
            e.printStackTrace();
        } catch (OutOfBoundsException e) {
            e.printStackTrace();
        }
        try {
            this.test.Application(Operation.DIV);
        } catch (DivisionZeroException e) {
            e.printStackTrace();
        } catch (EmptyStackException e) {
            e.printStackTrace();
        } catch (OutOfBoundsException e) {
            e.printStackTrace();
        }
        assertTrue(this.test.Retourner().get(this.test.Retourner().size()-1)==2.5);
    }

    @Test(expected = DivisionZeroException.class )
    public void DivisionZeroTest()throws DivisionZeroException{
        try {
            this.test.Enregistrer(2);
            this.test.Enregistrer(0);
        } catch (FullStackException e) {
            e.printStackTrace();
        } catch (OutOfBoundsException e) {
            e.printStackTrace();
        }
        try {
            this.test.Application(Operation.DIV);
        }  catch (EmptyStackException e) {
            e.printStackTrace();
        } catch (OutOfBoundsException e) {
            e.printStackTrace();
        }


    }
    @Test(expected = EmptyStackException.class )
    public void EmptyStackTest() throws DivisionZeroException, EmptyStackException, OutOfBoundsException {

            this.test.Application(Operation.DIV);

    }

    @Test(expected = FullStackException.class )
    public void FullStackTest() throws DivisionZeroException, EmptyStackException, FullStackException, OutOfBoundsException {
        for(int i =0; i<101;i++){

            this.test.Enregistrer(i);

        }

    }


    @Test(expected = OutOfBoundsException.class )
    public void OutOfBoundsTest() throws DivisionZeroException, EmptyStackException, FullStackException, OutOfBoundsException {

            this.test.Enregistrer(100000000);

    }


    @Test(expected = OutOfBoundsException.class )
    public void OutOfBoundsNegativeTest() throws DivisionZeroException, EmptyStackException, FullStackException, OutOfBoundsException {

            this.test.Enregistrer(-100000000);

    }

    @Test(expected = OutOfBoundsException.class )
    public void HighResultTest() throws  OutOfBoundsException {

        try {
            this.test.Enregistrer(1);
            this.test.Enregistrer(100000);
        } catch (FullStackException e) {
            e.printStackTrace();
        } catch (OutOfBoundsException e) {
            e.printStackTrace();
        }
        try {
            this.test.Application(Operation.PLUS);
        } catch (DivisionZeroException e) {
            e.printStackTrace();
        } catch (EmptyStackException e) {
            e.printStackTrace();
        }

    }
}