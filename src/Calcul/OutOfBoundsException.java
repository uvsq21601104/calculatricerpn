package Calcul;

public class OutOfBoundsException extends InputException {

    public OutOfBoundsException() {
        super("Value is either superior to MAX_VALUE or inferior to MIN_VALUE");
    }


}
