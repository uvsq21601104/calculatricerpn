package Calcul;

import Calcul.*;

import java.util.Scanner;

import static java.lang.System.exit;


public class SaisieRPN {


    private MoteurRPN moteur;
    private Scanner scanner;
    public SaisieRPN(MoteurRPN moteur){

        this.moteur=moteur;
        this.scanner=new Scanner(System.in);

    }

    public void getInput() throws InvalidCharacterException {


            String in =new String();
            in=this.scanner.next();
            if(in.equals("exit")) exit(0);
            else if(in.equals("+") ){

                try {
                    moteur.Application(Operation.PLUS);
                } catch (DivisionZeroException e) {
                    e.printStackTrace();
                } catch (EmptyStackException e) {
                    e.printStackTrace();
                } catch (OutOfBoundsException e) {
                    e.printStackTrace();
                }


            }
            else if(in.equals("-") ){

                try {
                    moteur.Application(Operation.MOINS);
                } catch (DivisionZeroException e) {
                    e.printStackTrace();
                } catch (EmptyStackException e) {
                    e.printStackTrace();
                } catch (OutOfBoundsException e) {
                    e.printStackTrace();
                }


            }
            else if(in.equals("*") ){

                try {
                    moteur.Application(Operation.MULT);
                } catch (DivisionZeroException e) {
                    e.printStackTrace();
                } catch (EmptyStackException e) {
                    e.printStackTrace();
                } catch (OutOfBoundsException e) {
                    e.printStackTrace();
                }


            }
            else if(in.equals("/") ){

                try {
                    moteur.Application(Operation.DIV);
                } catch (DivisionZeroException e) {
                    e.printStackTrace();
                } catch (EmptyStackException e) {
                    e.printStackTrace();
                } catch (OutOfBoundsException e) {
                    e.printStackTrace();
                }


            }

            else if(in.matches("\\p{Digit}+|-\\p{Digit}+")) {
                try {
                    moteur.Enregistrer(Integer.parseInt(in));
                } catch (FullStackException e) {
                    e.printStackTrace();
                } catch (OutOfBoundsException e) {
                    e.printStackTrace();
                }
            }
            else{

                throw new InvalidCharacterException();

            }

    }




}
