package Calcul;

public class InvalidCharacterException extends InputException {

    public InvalidCharacterException() {
        super("Your input contain an invalid character");
    }


}
