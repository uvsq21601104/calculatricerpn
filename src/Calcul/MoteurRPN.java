package Calcul;

import Calcul.DivisionZeroException;
import Calcul.EmptyStackException;
import Calcul.FullStackException;

import java.util.*;

public class MoteurRPN {

    private List<Double> stack = new ArrayList<Double>();
    private int tailleMax=100;
    private final static int MAX_VALUE=100000;
    private final static int MIN_VALUE=0;

    public MoteurRPN(){



    }

    public void Enregistrer(double op) throws FullStackException, OutOfBoundsException {
        int s=stack.size();
        if(s == tailleMax) throw new FullStackException();
        else {

            if(Math.abs(op)>=MIN_VALUE && Math.abs(op) <=MAX_VALUE) {
                stack.add(s, op);
                Return();
            }
            else throw new OutOfBoundsException();
        }

    }

    public double Depiler()throws Calcul.EmptyStackException {

       if(stack.size()==0)throw new Calcul.EmptyStackException();
       else {
           double res = stack.get(stack.size() - 1);
           stack.remove(stack.size() - 1);
           return res;
       }
    }

    public void Application(Operation operation) throws DivisionZeroException, EmptyStackException, OutOfBoundsException {


        double i= 0;
          if(stack.size()>1) {
              i = operation.eval(this.Depiler(), this.Depiler());

              if(Math.abs(i)<MIN_VALUE || Math.abs(i)>MAX_VALUE) throw new OutOfBoundsException();

          }
          else
              throw new EmptyStackException();
        stack.add(i);
        Return();

    }

    public List<Double> Retourner(){


        return this.stack;
    }

    public void Return(){

        String expression =new String();
        for(double e: stack){
            expression += e;
            expression += " ";

        }
        System.out.println(expression);

    }
    public int Size(){

        return this.stack.size();

    }

}
