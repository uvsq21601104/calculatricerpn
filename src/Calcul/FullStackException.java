package Calcul;

public class FullStackException extends StackException {

    public FullStackException() {
        super("Stack is full");
    }


}
