package Calcul;

public class EmptyStackException extends StackException {

    public EmptyStackException() {
        super("Stack is empty or not enough value");
    }

}
